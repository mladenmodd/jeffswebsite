from django.apps import AppConfig


class YelpscraperConfig(AppConfig):
    name = 'yelpscraper'
